# https://docs.docker.com/engine/examples/dotnetcore/#create-a-dockerfile-for-an-aspnet-core-application
FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy everything else and build
COPY . ./
RUN dotnet restore

# Run tests
RUN dotnet test

RUN dotnet publish --configuration Release --output out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/App/out .
# Expose don't actually publish the port. It's just for documentation. This is the port I intend to publish
EXPOSE 80
ENTRYPOINT ["dotnet", "k8sfirstapp.dll"]
